# Module dataforge-data



## Usage

## Artifact:

The Maven coordinates of this project are `space.kscience:dataforge-data:0.10.0`.

**Gradle Kotlin DSL:**
```kotlin
repositories {
    maven("https://repo.kotlin.link")
    mavenCentral()
}

dependencies {
    implementation("space.kscience:dataforge-data:0.10.0")
}
```
